from functools import wraps
from fastapi import HTTPException,Request,Depends,Header
from database import get_db
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from models.account import User,BlockUser,CustomizedSecretKey,Token
from sqlalchemy import select
from jose import jwt
import time

ALGOITHM="HS256"


def notLoginRequired(func):
    @wraps(func)
    async def inner(request:Request,*args,**kwargs):
        t=request.headers.get("TOKEN")
        j=request.headers.get("Authorization")
        if t or j:
            raise HTTPException(detail="user already logged in ",status_code=404)
        return  await func(request,*args,**kwargs)
    return inner







async def notTokenInHeader(request:Request):
    t=request.headers.get("TOKEN")
    j=request.headers.get("Authorization")
    user_ip=request.client.host
    if t or j:
            raise HTTPException(detail="user already logged in ",status_code=404)
    return user_ip

    


# async def userNotBeBlock(
#           request:Reques):
#      user_ip=request.client.host






async def IsuserLoggedIn(
          request,
          db,
):
     user_ip=request.client.host
     c=await db.execute(
          select(CustomizedSecretKey).where(
               CustomizedSecretKey.user_ip==user_ip).limit(1)
     )
     c_sc=c.scalar()
     if not c_sc:
          return False
     else:
          t=request.headers.get("TOKEN")
          j=request.headers.get("Authorization")
          if t and j :
      
               t=await db.execute(
                 select(Token).where(Token.key==t)
            )
               
               t_sc=t.scalar()

               if c_sc and t_sc:
                    d=jwt.decode(t,key=c_sc.key,algorithms=ALGOITHM)
                    now=time.time()
                    if d and d.get("id") and d.get("sub") and d.get("exp") > now:
                        return True,d.get("id")
               
               return False
          
          return False
          
          
          