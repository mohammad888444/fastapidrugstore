from fastapi import (
    FastAPI,APIRouter,Depends,
    Header,HTTPException,
    Cookie,Query,Request,Response,
    Path,File,UploadFile,Form,BackgroundTasks
)
from fastapi.responses import JSONResponse,HTMLResponse,RedirectResponse
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles
from fastapi.security import OAuth2PasswordBearer
# from decorator import notLoginRequired

from utils import (
    generate_hash_password,
    createCustomHeader,makeUserToken,
    send_otp,creatTokenForOtp
)
import time
from datetime import datetime,timedelta

 

from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy import select,delete,update


from database import get_db
from schema.user import UserRegister,UserLogin


# from .errors import RateLimitExceeded
from slowapi import Limiter, _rate_limit_exceeded_handler
from slowapi.util import get_remote_address

from decorator import notLoginRequired,notTokenInHeader

from jose import jwt,JWTError


router=APIRouter()
limiter = Limiter(key_func=get_remote_address)
token=OAuth2PasswordBearer(tokenUrl="/token")

template=Jinja2Templates(directory="templates",)


ALGOITHM="HS256"


@router.post(
    path="/",
    description="register and login route ",
    summary="register and login path",
    response_class=JSONResponse,
    response_description="register and login form",
    response_model=dict,
    deprecated=False,
    include_in_schema=True,
    dependencies=[Depends(notTokenInHeader)]
)
@limiter.limit("5/minute")
@notLoginRequired
async def registerAndLogin(
    request:Request,
    userinfo:UserRegister,
    bg:BackgroundTasks,
    db:AsyncSession=Depends(get_db)
):
    from models.account import User,BlockUser,CustomizedSecretKey,Token,UserOtp
    from models.cart import Cart

    user_ip=request.client.host
    b=await db.execute(
        select(BlockUser).where(BlockUser.user_ip==user_ip).limit(1)
    )

    if b.scalar():
        raise HTTPException(detail="user is blocked",status_code=404)
    
    u=await db.execute(
        select(User).where(User.phone==userinfo.phone).limit(1)
    )
    a_sc=u.scalar()
    
    if not a_sc:

        new_user=User(
            phone=userinfo.phone,
            is_active=True
        )

        db.add(new_user)
        await db.commit()
        await db.refresh(new_user)

        now=time.time()

        new_cart=Cart(
            cart_id_unique=str(new_user.id)
        )
        db.add(new_cart)
        await db.commit()
        await db.refresh(new_cart)


        # new_user.password=await generate_hash_password(str(now),str(new_user.id))

        # await db.commit()
        # await db.refresh(new_user)

        token=Token(
            user_id=new_user.id,
            key=await makeUserToken(),
        )
        db.add(token)
        await db.commit()
        await db.refresh(token)


        user_ip=request.client.host
        c=await createCustomHeader()
        user_header=CustomizedSecretKey(
        key=c,
        
        user_id=new_user.id,
        user_ip=user_ip
    )
        db.add(user_header)

        await db.commit()
        await db.refresh(user_header)
        o=await creatTokenForOtp()
        now_plus=time.time()+8000
        real_now=datetime.fromtimestamp(now_plus)

        all_o=UserOtp.__table__.delete().where(UserOtp.user_id==new_user.id)
        await db.execute(all_o)
        await db.commit()


        new_otp=UserOtp(
            user_id=new_user.id,
            otp=o,
            expire=real_now,
        )

        db.add(new_otp)
        await db.commit()
        await db.refresh(new_otp)


        bg.add_task(send_otp,new_user.phone,new_otp.otp)
        

        return {
            "send":True,
            "token":str(token.key)
        }

    else:
        
        all_o=UserOtp.__table__.delete().where(UserOtp.user_id==a_sc.id)
        await db.execute(all_o)
        await db.commit()
        
        o=await creatTokenForOtp()
        now_plus=time.time()+8000
        real_now=datetime.fromtimestamp(now_plus)

        new_otp=UserOtp(
            user_id=a_sc.id,
            otp=o,
            expire=real_now,
        )

        db.add(new_otp)
        await db.commit()
        await db.refresh(new_otp)

        t=await db.execute(
            select(Token).where(Token.user_id==a_sc.id).limit(1)
        )

        token=t.scalar()
        bg.add_task(send_otp,a_sc.phone,new_otp.otp)


        return {
            "send":True,
            "token":str(token.key)
        }








async def TokenAuthentication(TOKEN:str=Header(
            title="token",
            description="custom token authentication",
            alias="TOKEN",
            min_length=40
        ),
        db:AsyncSession=Depends(get_db)
):
    from models.account import Token
    
    t=await db.execute(
        select(Token).where(Token.key==TOKEN).limit(1)
    )
    t_sc=t.scalar()
    if not t_sc:
        raise HTTPException(detail="token not found",status_code=404)
    return str(t_sc.key)

    




async def jwtPermission(
        user_id,user_ip,user_token,db,time=None
):
    from models.account import CustomizedSecretKey
    

    data={"id":user_id,"sub":user_token}

    c=await db.execute(
        select(CustomizedSecretKey).where(CustomizedSecretKey.user_ip==user_ip).limit(1)
    )
    c_sc=c.scalar()

    if not c_sc:
        raise HTTPException(detail="user must have special token ",status_code=404)
    
    t=None
    if time:
        t=time
    else:
        t=datetime.utcnow()+timedelta(minutes=5555)
    
    data.update({"exp":t})

    return jwt.encode(data,key=c_sc.key,algorithm=ALGOITHM)




    




@router.post(
    path="/login",
    description="login router",
    summary="login router",
    response_class=JSONResponse,
    response_model=dict,
    deprecated=False,
    include_in_schema=True,
    dependencies=[Depends(notTokenInHeader)],
    response_description="login and get jwt token"
)
@limiter.limit("5/minute")
async def login(
    request:Request,
    user_login:UserLogin,
    token_user:str=Depends(TokenAuthentication),
    db:AsyncSession=Depends(get_db)
):
    from models.account import Token,UserOtp
    
    t=await db.execute(
        select(Token).where(Token.key==token_user).limit(1)
    )

    t_sc=t.scalar()
    
    o=await db.execute(
        select(UserOtp).where(
            UserOtp.otp==user_login.code
            ).limit(1)
    )

    o_sc=o.scalar()

    if o_sc.user_id!=t_sc.user_id:
        raise HTTPException(detail="not allowed",status_code=404)
    
    ip=request.client.host
    now=datetime.utcnow()+timedelta(minutes=5555)

    j=await jwtPermission(
        user_id=t_sc.user_id,
        user_ip=ip,
        user_token=t_sc.key,
        time=now
    )    

    return {
        "j":j,
        "token":t_sc.key,        
    }





async def oauthAuthentication(
        request:Request,
        token:str=Depends(token),
        db:AsyncSession=Depends(get_db)
):
    from models.account import CustomizedSecretKey
    
    user_ip=request.client.host
    t=await db.execute(
        select(CustomizedSecretKey).where(CustomizedSecretKey.user_ip==user_ip).limit(1)
    )

    t_sc=t.scalar()
    if not t_sc:
        raise HTTPException(detail="user not found",status_code=404)
    try:
        res=jwt.decode(token,t_sc.key,algorithms=ALGOITHM)
        now=time.time()
        if res and res.get("id") and res.get("sub") and res.get("exp") > now:
            return {
                "id":str(res.get("id")),
                "token":res.get("sub")
            }
        else:
            raise HTTPException(detail="token not found")
    except Exception as e:
        raise HTTPException(detail="token is invalid",status_code=404)






async def customAuthentication(
        request:Request,
        TOKEN:str=Depends(TokenAuthentication),
        jwt:dict=Depends(oauthAuthentication),
        db:AsyncSession=Depends(get_db)
):
    from models.account import Token
    
    t=await db.execute(
        select(Token).where(Token.key==TOKEN)
    )
    t_sc=t.scalar()
    if t_sc.user_id!=jwt.get("token"):
        raise HTTPException(detail="not authenticated user",status_code=404)
    
    return {
        "token":t_sc.key,
        "jwt":jwt
    }






 


  




# @router.get(
#     path="/loginRegister",
# )

# async def loginRegisterTemlplate(
#     request:Request,
# ):
#     context={
#         'request':request,
#         'name':"alireza"
#     }

#     return template.TemplateResponse(
#         name="account/login.html",
#         context=context
#     )



 

# @router.post(
#     path="/log",
#     response_class=JSONResponse,
#     response_model=dict
# )
# @limiter.limit("5/minute")
# async def log(
#     request:Request,
#     bg:BackgroundTasks,
#     phone:str=Form(
#         # max_length=11,
#         # min_length=10,
#         # title="phone",
#         # alias="phone",
#         # description="enter phone number",
#         # max_digits=11
#     ),
#     db:AsyncSession=Depends(get_db)
# ):
#     from models.account import Token,BlockUser,UserOtp,User,CustomizedSecretKey
    
#     user_ip=request.client.host
#     b=await db.execute(
#         select(BlockUser).where(BlockUser.user_ip==user_ip).limit(1)
#     )

#     if b.scalar():
#         raise HTTPException(detail="user is blocked",status_code=404)
    
#     u=await db.execute(
#         select(User).where(User.phone==phone).limit(1)
#     )
#     a_sc=u.scalar()
    
#     if not a_sc:

#         print("NONONONONO")
#         print("NONONONONO")
#         print("NONONONONO")
        
#         new_user=User(
#             phone=phone,
#             is_active=True,
#             user_ip=user_ip,
#             password="Mohammad444$",
#             is_admin=False,
#             is_superuser=False,
#         )

#         db.add(new_user)
#         await db.commit()
#         await db.refresh(new_user)
#         now=time.time()

#         # new_user.password=await generate_hash_password(str(new_user.id))

#         # await db.commit()
#         # await db.refresh(new_user)

#         k=await makeUserToken()
#         token=Token(
#             user_id=new_user.id,
#             key=k
#         )

#         db.add(token)

#         await db.commit()
#         await db.refresh(token)


#         user_ip=request.client.host
#         c=await createCustomHeader()
#         user_header=CustomizedSecretKey(
#             key=c,
#             user_id=new_user.id,
#             user_ip=user_ip
#         )
#         db.add(user_header)

#         await db.commit()
#         await db.refresh(user_header)
#         o=await creatTokenForOtp()
        

#         all_o=UserOtp.__table__.delete().where(UserOtp.user_id==new_user.id)
#         await db.execute(all_o)
#         await db.commit()
        
        
#         now=datetime.now()+timedelta(minutes=2)
 

#         new_otp=UserOtp(
#             user_id=new_user.id,
#             otp=o,
#             expire=now,
#         )

#         db.add(new_otp)
#         await db.commit()
#         await db.refresh(new_otp)


#         bg.add_task(send_otp,new_user.phone,new_otp.otp)
        
#         # context={
#         #     'request':request,
#         #     "send":True,
#         #     "token":str(token.key)
#         # }

#         return {
#             "send":True,
#             "token":str(token.key)
#         }

#         # return template.TemplateResponse("account/login.html",context)

#     else:
#         print("UUUUUUUUUUUu")
#         print("UUUUUUUUUUUu")
#         print("UUUUUUUUUUUu")
#         print("UUUUUUUUUUUu")
        
#         all_o=UserOtp.__table__.delete().where(UserOtp.user_id==a_sc.id)
#         await db.execute(all_o)
#         await db.commit()

#         o=await creatTokenForOtp()
#         now_plus=time.time()+8000
#         real_now=datetime.fromtimestamp(now_plus)
        

#         now=datetime.now()+timedelta(minutes=2)

#         new_otp=UserOtp(
#             user_id=a_sc.id,
#             otp=o,
#             expire=now,
#         )

#         db.add(new_otp)
#         await db.commit()
#         await db.refresh(new_otp)

#         t=await db.execute(
#             select(Token).where(Token.user_id==a_sc.id).limit(1)
#         )

#         token=t.scalar()


#         # context={
#         #     'request':request,
#         #     "send":True,
#         #     "token":str(token.key)
#         # }

#         # return template.TemplateResponse("account/login.html",context)

#         bg.add_task(send_otp,a_sc.phone,new_otp.otp)

#         return {
#             "send":True,
#             "token":str(token.key)
#         }







    


# @router.post(
#     path="/sigin",
#     description="sigin template router",
#     summary="sigin template",
#     deprecated=False,
#     include_in_schema=True,
#     status_code=200,
#     response_class=HTMLResponse,
#     response_description="sigin template for...",
#     tags=["Sigin Template"],
#     # dependencies=[Depends(TokenAuthentication)]
# )

# async def sigin(
#     request:Request,
#     token:str=Depends(TokenAuthentication),
#     code:str=Form
#     (
#         title="code verification",
#         description="code verification",
#         alias="code",
#         max_length=5,
#         min_length=5,
#     ),
#     db:AsyncSession=Depends(get_db),
# ):
    
#     t=await db.execute(
#         select(Token).where(Token.key==token).limit(1)
#     )
#     t_sc=t.scalar()
#     now=datetime.now()

#     o=await db.execute(
#         select(UserOtp).where(UserOtp.expire>now,
#                               UserOtp.otp==code).limit(1)
#     )
#     o_sc=o.scalar()

#     if t_sc.user_id!=o_sc.user_id:
#         raise HTTPException(detail="user isnot allowed")
    
#     user_ip=request.client.host

#     new_j=await jwtPermission(
#         user_id=t_sc.user_id,
#         user_ip=user_ip,
#         user_token=t_sc.key,
#         db=db,
#         # time=
#     )

#     return {
#         "token":t_sc.key,
#         "jwt":new_j
#     }







