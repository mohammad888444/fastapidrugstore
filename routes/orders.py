from fastapi import (
    FastAPI,APIRouter,Depends,
    Header,HTTPException,
    Cookie,Query,Request,Response,
    Path,File,UploadFile,Form,BackgroundTasks
)
from fastapi.responses import JSONResponse,HTMLResponse,RedirectResponse

from utils import (
    generate_hash_password,
    createCustomHeader,makeUserToken,
    send_otp,creatTokenForOtp,
    getData,convert_str_to_uuid
)

from datetime import datetime,timedelta
import uuid

 

from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy import select,delete,update,func,and_,desc,asc,text,case,or_


from database import get_db


from slowapi import Limiter
from slowapi.util import get_remote_address

from decorator import notLoginRequired,notTokenInHeader,IsuserLoggedIn
from fastapi_pagination import Page, add_pagination, paginate,Params
from schema.category import SecondCategorySchema,ThirdCategorySchema,CategorySchema,ProductSchem,ProductSchem2Detail,ReviewProductSchema
from routes.account import customAuthentication

from schema.cart import CoponSchema
import random
import json
import requests
from schema.order import UpdateOrderSchema
from suds.client import Client


router=APIRouter()
limiter = Limiter(key_func=get_remote_address)


add_pagination(router)





MMERCHANT_ID = '8c00df17-3d8e-4b97-8ff7-dee5e02e945f'  # Required
ZARINPAL_WEBSERVICE = 'https://www.zarinpal.com/pg/services/WebGate/wsdl'  # Required
amount = 100000  # Amount will be based on Toman  Required
description = u'توضیحات تراکنش تستی'  # Required
email = 'user@userurl.ir'  # Optional
mobile = '09123456789'  # Optional
ZP_API_REQUEST = f"https://sandbox.zarinpal.com/pg/rest/WebGate/PaymentRequest.json"
ZP_API_VERIFY = f"https://sandbox.zarinpal.com/pg/rest/WebGate/PaymentVerification.json"
ZP_API_STARTPAY = f"https://sandbox.zarinpal.com/pg/StartPay/"


@router.post(
    path="/checkoutOrder/{id}",
    description="make order payment",
    summary="make order",
    response_class=JSONResponse,
    deprecated=False,
    include_in_schema=True
)

async def make(
    request:Request,
    ors:UpdateOrderSchema,
    id:uuid.UUID=Path(
        title="order id"
    ),
    
    auth_:dict=Depends(customAuthentication),
    db:AsyncSession=Depends(get_db),
):
    
    from models.orders import Copon,Order
    from models.cart import CartItem
    from models.category import Product
    from models.account import Token

    token=await db.execute(
        select(Token).where(Token.key==auth_.get("token")).limit(1)
    )

    token_sc=token.scalar()


    
    o=await db.execute(
            select(
                Order
            ).where(
                Order.is_paid==False,
                Order.user_id==token_sc.user_id,
                Order.id==await convert_str_to_uuid(id)
            )
        )

    o_sc=o.scalar()


    CallbackURL = f'http://127.0.0.1:8080/verify/?token={auth_.get("token")}&order_id={o_sc.id}'


    user_carts=await db.execute(
        select(CartItem).where(
            CartItem.user_id==token_sc.user_id,
            CartItem.is_paid==False
        )
    )
    user_carts_sc=user_carts.scalars().unique().all()

    o_sc.address=ors.address
    o_sc.email=ors.email.lower()
    o_sc.name=ors.name
    await db.commit()

    for i in user_carts_sc:
        o_sc.cartitems.append(i)
        await db.commit()
    await db.commit()

    data = {
        "MerchantID": MMERCHANT_ID,
        "Amount": amount,
        "Description": description,
        "Phone": mobile,
        "CallbackURL": CallbackURL,
    }
    data = json.dumps(data)
    # set content length by data
    headers = {'content-type': 'application/json', 'content-length': str(len(data)) }
    try:
        response = requests.post(ZP_API_REQUEST, data=data,headers=headers, timeout=10)

        if response.status_code == 200:
            response = response.json()
            if response['Status'] == 100:

                o_sc.pay_link=ZP_API_STARTPAY + str(response['Authority'])
                o_sc.authority=str(response['Authority'])
                await db.commit()

                return {'status': True, 
                        'url': ZP_API_STARTPAY + str(response['Authority']),
                          'authority': response['Authority']
                          }
            else:
                return {'status': False, 'code': str(response['Status'])}
        return response
    
    except requests.exceptions.Timeout:
        return {'status': False, 'code': 'timeout'}
    except requests.exceptions.ConnectionError:
        return {'status': False, 'code': 'connection error'}






    



@router.post(
    path="/verify/{id}",
    description="verify paid ",
    response_class=JSONResponse,
    response_description="verify order status",
    include_in_schema=True,
    deprecated=False,

)

async def verifyOrder(
    request:Request,
    ors:UpdateOrderSchema,
    id:uuid.UUID=Path(
        title="order id"
    ),
    token:str=Query(
        title="user token key",
        description="tokn key"
    ),
    db:AsyncSession=Depends(get_db),
):
    

    from models.orders import Copon,Order,OrderProduct,Payment
    from models.cart import CartItem
    from models.category import Product
    from models.account import Token

    if not token:
        raise HTTPException("token is required",status_code=404)

    token=await db.execute(
        select(Token).where(Token.key==token).limit(1)
    )

    token_sc=token.scalar()

    if not token_sc:
        raise HTTPException("user token not found is required",status_code=404)
    
    o=await db.execute(
            select(
                Order
            ).where(
                Order.is_paid==False,
                Order.user_id==token_sc.user_id,
                Order.id==await convert_str_to_uuid(id)
            ).limit(1)
        )

    o_sc=o.scalar()




    data = {
        "MerchantID": MMERCHANT_ID,
        "Amount": o_sc.amount_with_copon,
        "Authority": o_sc.authority,
    }
    data = json.dumps(data)
    # set content length by data
    headers = {'content-type': 'application/json', 'content-length': str(len(data)) }
    response = requests.post(ZP_API_VERIFY, data=data,headers=headers)

    if response.status_code == 200:
        response = response.json()
        if response['Status'] == 100:

            o_sc.is_paid=True

            await db.commit()


            user_carts=await db.execute(
                    select(CartItem).where(
                        CartItem.user_id==token_sc.user_id,
                        CartItem.is_paid==False
                    )
                )
            user_carts_sc=user_carts.scalars().unique().all()

            cs=update(CartItem).where(
                CartItem.user_id==token_sc.user_id,
                CartItem.is_paid==False
            ).values(
                is_paid=True
            )
            await db.execute(cs)
            await db.commit()

            p=Payment(
                user_id=token_sc.user_id,
                order=o_sc,
                is_paid=True,
                bank_status='good',
                tracking_code=response['RefID'],
                bank_name=response["bank_name"],

            )

            db.add(p)
            await db.commit(p)
            await db.refresh(p)


            for i in user_carts_sc:
                new_order_product=OrderProduct(
                    user_id=token_sc.user_id,
                    order_id=o_sc.id,
                    product_id=i.product_id,
                    quantity=i.quantity
                )
                db.add(new_order_product)
                await db.commit(new_order_product)
                await db.refresh(new_order_product)
            
            return {'status': True, 'RefID': response['RefID']}
        
        else:
            return {'status': False, 'code': str(response['Status'])}
    return response