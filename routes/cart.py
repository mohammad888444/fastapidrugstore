from fastapi import (
    FastAPI,APIRouter,Depends,
    Header,HTTPException,
    Cookie,Query,Request,Response,
    Path,File,UploadFile,Form,BackgroundTasks
)
from fastapi.responses import JSONResponse,HTMLResponse,RedirectResponse

from utils import (
    generate_hash_password,
    createCustomHeader,makeUserToken,
    send_otp,creatTokenForOtp,
    getData,convert_str_to_uuid
)

from datetime import datetime,timedelta
import uuid

 

from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy import select,delete,update,func,and_,desc,asc,text,case,or_


from database import get_db


from slowapi import Limiter
from slowapi.util import get_remote_address

from decorator import notLoginRequired,notTokenInHeader,IsuserLoggedIn
from fastapi_pagination import Page, add_pagination, paginate,Params
from schema.category import SecondCategorySchema,ThirdCategorySchema,CategorySchema,ProductSchem,ProductSchem2Detail,ReviewProductSchema
from routes.account import customAuthentication

from schema.cart import CoponSchema
import random




router=APIRouter()
limiter = Limiter(key_func=get_remote_address)


add_pagination(router)




@router.get(
    path="/my",
    description="user cart",
    response_class=JSONResponse,
    response_description="return user cart aggreagte",
    # response_model=dict,
    deprecated=False,
    include_in_schema=True,
    summary="return user cart aggregate"
)

async def userCartAgg(
    request:Request,
    # auth_:dict=Depends(customAuthentication),
    db:AsyncSession=Depends(get_db)
):  
    from models.cart import Cart,CartItem
    from models.account import User,Token
    from models.category import Product
    token="6228631015581189609208862362391705763041422898353627504191691718717257"
    
    c=await db.execute(
        select(Token).where(Token.key==token)
    )
    c_sc=c.scalar()
    agg=await db.execute(
        select(
                func.sum(CartItem.quantity*Product.price).label("aa")
            ).join(
                Product
            ).where(
            CartItem.user_id==c_sc.user_id,
            CartItem.is_paid==False
        )
    )   
 
    i_sc=agg.scalar()

    items=await db.execute(
        select(CartItem).where(CartItem.user_id==c_sc.user_id,
            CartItem.is_paid==False).order_by("created").limit(10)
    )

    items_sc=items.scalars().unique().all()



    return {
        "okay":i_sc,
        'items_sc':items_sc
    }






@router.post(
    path="/add/{id}",
    description="add item",
    response_class=JSONResponse,
    response_description="add new item" ,
    deprecated=False,
    include_in_schema=True,
    summary="add new item",
    response_model=dict,
    # dependencies=[Depends()]
)


async def addNewItem(
    request:Request,
    id:uuid.UUID=Path(
        title="product id",
        description="product id to add cart item",
    ),
    auth_:dict=Depends(customAuthentication),
    db:AsyncSession=Depends(get_db)
):
    
    from models.account import User,Token
    from models.category import  Product
    from models.cart import Cart,CartItem

    token=await db.execute(
        select(Token).where(Token.key==auth_.get("token")).limit(1)
    )

    p=await db.execute(
        select(Product).where(
                        Product.id==id,
                        Product.is_active==True
            )
    )
    p_sc=p.scalar()
    token_sc=token.scalar()

    c=await db.execute(
        select(CartItem).where(

            CartItem.is_paid==False,
            CartItem.product_id==p_sc.id,
            CartItem.user_id==token_sc.id
        ).limit(1)
    )
    c_sc=c.scalar()

    if c_sc:
        c_sc.quantity+=1
        await db.commit()
        await db.refresh(c_sc)
    
    else:

        c=await db.execute(
            select(Cart).where(
                Cart.cart_id_unique==await convert_str_to_uuid(token_sc.user_id)
            )
        )

        new_c=CartItem(
            user_id=token_sc.user_id,
            cart_id=c.scalar().id,
            product_id=p_sc.id,
            quantity=1
        )

        db.add(new_c)
        await db.commit()
        await db.refresh(new_c)

        agg=await db.execute(
            select(
                    func.sum(CartItem.quantity*Product.price).label("aa")
                ).join(
                    Product
                ).where(
                CartItem.user_id==c_sc.user_id,
                CartItem.is_paid==False
            )
        )   
    
        i_sc=agg.scalar()

        return {
            "id":new_c.id,
            "quantity":new_c.quantity,
            "name":p_sc.name,
            "price":p_sc.price,
            'agg':i_sc
        }


    agg=await db.execute(
            select(
                    func.sum(CartItem.quantity*Product.price).label("aa")
                ).join(
                    Product
                ).where(
                CartItem.user_id==c_sc.user_id,
                CartItem.is_paid==False
            )
        )   
    
    i_sc=agg.scalar()

    return {
        "id":c_sc.id,
        "quantity":c_sc.quantity,
        "name":p_sc.name,
        "price":p_sc.price,
        'agg':i_sc
    }









    






@router.post(
    path="/more/{c_id}",
    description="cart item more button",
    summary="cart item more button",
    deprecated=False,
    include_in_schema=True,
    response_class=JSONResponse,
    response_description="cart item more",
    response_model=dict
)

async def moreButton(
    request:Request,
    c_id:uuid.UUID=Path(
        title="cart item id",
        description="cart item id"
    ),
    auth_:dict=Depends(customAuthentication),
    db:AsyncSession=Depends(get_db),
):
    
    from models.account import User,Token
    from models.category import  Product
    from models.cart import Cart,CartItem

    token=await db.execute(
        select(Token).where(Token.key==auth_.get("token")).limit(1)
    )

    token_sc=token.scalar()
    c=await db.execute(
        select(CartItem).where(
                CartItem.id==c_id,
                CartItem.user_id==token_sc.user_id,
                CartItem.is_paid==False,
            ).limit(1)
    )

    c_sc=c.scalar()

    if c_sc:
        c_sc.quantity+=1
        await db.commit()
        await db.refresh(c_sc)

        agg=await db.execute(
            select(
                    func.sum(CartItem.quantity*Product.price).label("aa")
                ).join(
                    Product
                ).where(
                CartItem.user_id==c_sc.user_id,
                CartItem.is_paid==False
            )
        )   
    
        i_sc=agg.scalar()
        return {
                "id":c_sc.id,
                "quantity":c_sc.quantity,
                # "name":p_sc.name,
                # "price":p_sc.price,
                'agg':i_sc
        }

    else:
        raise HTTPException(detail="cart item not found",status_code=404)
    







@router.post(
    path="/less/{c_id}",
    description="cart item less button",
    summary="cart item less button",
    deprecated=False,
    include_in_schema=True,
    response_class=JSONResponse,
    response_description="cart item less",
    response_model=dict
)

async def lessButton(
    request:Request,
    c_id:uuid.UUID=Path(
        title="cart item id",
        description="cart item id"
    ),
    auth_:dict=Depends(customAuthentication),
    db:AsyncSession=Depends(get_db),
):
    
    from models.account import User,Token
    from models.category import  Product
    from models.cart import Cart,CartItem

    token=await db.execute(
        select(Token).where(Token.key==auth_.get("token")).limit(1)
    )

    token_sc=token.scalar()
    c=await db.execute(
        select(CartItem).where(
                CartItem.id==c_id,
                CartItem.user_id==token_sc.user_id,
                CartItem.is_paid==False,
            ).limit(1)
    )

    c_sc=c.scalar()

    if not c_sc:
        raise HTTPException(detail="cart item not found",status_code=404)

    if  c_sc.quantity>0:
        c_sc.quantity-=1
        await db.commit()
        await db.refresh(c_sc)

        agg=await db.execute(
            select(
                    func.sum(CartItem.quantity*Product.price).label("aa")
                ).join(
                    Product
                ).where(
                CartItem.user_id==c_sc.user_id,
                CartItem.is_paid==False
            )
        )   
    
        i_sc=agg.scalar()
        return {
                "id":c_sc.id,
                "quantity":c_sc.quantity,
                # "name":p_sc.name,
                # "price":p_sc.price,
                'agg':i_sc
        }

    else:

        dels=CartItem.__table__.delete().where(
            CartItem.id==c_id,
            CartItem.user_id==token_sc.user_id,
            CartItem.is_paid==False,
        ).limit(1)

        await db.execute(dels)
        await db.commit()

        agg=await db.execute(
            select(
                    func.sum(CartItem.quantity*Product.price).label("aa")
                ).join(
                    Product
                ).where(
                CartItem.user_id==c_sc.user_id,
                CartItem.is_paid==False
            )
        )   
    
        i_sc=agg.scalar()


        return {
            "deleted":True,
            "agg":i_sc
        }




        
    
@router.post(
    path="/remove/{c_id}",
    description="cart item remove button",
    summary="cart item remove button",
    deprecated=False,
    include_in_schema=True,
    response_class=JSONResponse,
    response_description="cart item remove",
    response_model=dict,
    
)
async def removeCart(
    request:Request,
    c_id:uuid.UUID=Path(
        title="cart item id",
        description="cart item id"
    ),
    auth_:dict=Depends(customAuthentication),
    db:AsyncSession=Depends(get_db),
):
    
    from models.account import User,Token
    from models.category import  Product
    from models.cart import Cart,CartItem

    token=await db.execute(
        select(Token).where(Token.key==auth_.get("token")).limit(1)
    )

    token_sc=token.scalar()

    c=await db.execute(
        select(CartItem).where(
                CartItem.id==c_id,
                CartItem.user_id==token_sc.user_id,
                CartItem.is_paid==False,
            ).limit(1)
    )

    c_sc=c.scalar()

    if not c_sc:
        raise HTTPException(detail="cart item not found",status_code=404)
    


    dels=CartItem.__table__.delete().where(
            CartItem.id==c_id,
            CartItem.user_id==token_sc.user_id,
            CartItem.is_paid==False,
        ).limit(1)

    await db.execute(dels)
    await db.commit()

    agg=await db.execute(
            select(
                    func.sum(CartItem.quantity*Product.price).label("aa")
                ).join(
                    Product
                ).where(
                CartItem.user_id==c_sc.user_id,
                CartItem.is_paid==False
            )
        )  
    i_sc=agg.scalar()


    return {
            "deleted":True,
            "agg":i_sc
        }






@router.post(
    path="/copon",
    description="use copon",
    summary="use copon",
    include_in_schema=True,
    deprecated=False,
    response_class=JSONResponse,
    response_description="return response that copon is worked or not",
    response_model=dict
)
async def copon(
    request:Request,
    code:CoponSchema,
    db:AsyncSession=Depends(get_db),
    auth_:dict=Depends(customAuthentication),
):
    from models.orders import Copon,Order
    from models.cart import CartItem
    from models.category import Product
    from models.account import Token

    token=await db.execute(
        select(Token).where(Token.key==auth_.get("token")).limit(1)
    )

    token_sc=token.scalar()




    now=datetime.now()

    c=await db.execute(

        select(Copon).where(
            Copon.expire>now,
            Copon.code==code.code,
            Copon.numbers>0
        ).limit(1)
    )
    c_sc=c.scalar()

    if not c_sc:
        raise HTTPException(detail="not correct",status_code=404)
    

    p=await db.execute(
        select(
            func.sum(CartItem.quantity*Product.price)
        ).join(
            Product
        ).where(
            CartItem.user_id==token_sc.user_id,
            CartItem.is_paid==False
        )
    )

    agg_sc=p.scalar()

    user_carts=await db.execute(
        select(CartItem).where(
            CartItem.user_id==token_sc.user_id,
            CartItem.is_paid==False
        )
    )
    user_carts_sc=user_carts.scalars().unique().all()



    new_order=Order(
        custom_order_id=''.join(random.choices([str(i) for i in range(0,10)])),
        user_id=token_sc.user_id,
        amount=agg_sc,
        amount_with_copon=(agg_sc*c_sc.percent)/100,
        used_copon=True)

    db.add(new_order)
    await db.commit()
    await db.refresh(new_order)

    return {
        "order_id":new_order.id
    }







@router.post(
    path="/checkout/{id:path}",
    description="checkout button",
    summary="button to make order or get order",
    response_class=JSONResponse,
    response_description="button checkout handle",
    deprecated=False,
    include_in_schema=True,
    response_model=dict,
)

async def checkout(
    request:Request,
    id,
    db:AsyncSession=Depends(get_db),
    auth_:dict=Depends(customAuthentication),
):
    from models.orders import Copon,Order
    from models.cart import CartItem
    from models.category import Product
    from models.account import Token

    token=await db.execute(
        select(Token).where(Token.key==auth_.get("token")).limit(1)
    )

    token_sc=token.scalar()
    o=None
    if id:

        o=await db.execute(
            select(
                Order
            ).where(
                Order.is_paid==False,
                Order.user_id==token_sc.user_id,
                Order.id==await convert_str_to_uuid(id)
            )
        )

        o_sc=o.scalar()

        return {
            "order_id":o_sc.id
        }
    
    else:
        p=await db.execute(
            select(
                func.sum(CartItem.quantity*Product.price)
            ).join(
                Product
            ).where(
                CartItem.user_id==token_sc.user_id,
                CartItem.is_paid==False
            )
        )

        agg_sc=p.scalar()

        # user_carts=await db.execute(
        #     select(CartItem).where(
        #         CartItem.user_id==token_sc.user_id,
        #         CartItem.is_paid==False
        #     )
        # )
        # user_carts_sc=user_carts.scalars().unique().all()



        new_order=Order(
            custom_order_id=''.join(random.choices([str(i) for i in range(0,10)])),
            user_id=token_sc.user_id,
            amount=agg_sc,
            amount_with_copon=agg_sc,
            used_copon=False
        )

        db.add(new_order)
        await db.commit()
        await db.refresh(new_order)

        return {
            "order_id":new_order.id
        }
        




    


# @router.get(
#     path="/hello/"
# )
@router.get(
    path="/hello/{id:path}"
)

async def test(id):
    print(id)
    return {
        "okay":True
    }