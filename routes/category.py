from fastapi import (
    FastAPI,APIRouter,Depends,
    Header,HTTPException,
    Cookie,Query,Request,Response,
    Path,File,UploadFile,Form,BackgroundTasks
)
from fastapi.responses import JSONResponse,HTMLResponse,RedirectResponse

from utils import (
    generate_hash_password,
    createCustomHeader,makeUserToken,
    send_otp,creatTokenForOtp,
    getData,convert_str_to_uuid
)
import time
from datetime import datetime,timedelta
import uuid
 

from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy import select,delete,update,func,and_,desc,asc,text,case,or_


from database import get_db
from schema.user import UserRegister,UserLogin


from slowapi.errors import RateLimitExceeded
from slowapi import Limiter, _rate_limit_exceeded_handler
from slowapi.util import get_remote_address

from decorator import notLoginRequired,notTokenInHeader,IsuserLoggedIn
from fastapi_pagination import Page, add_pagination, paginate,Params
from schema.category import SecondCategorySchema,ThirdCategorySchema,CategorySchema,ProductSchem,ProductSchem2Detail,ReviewProductSchema


from routes.account import customAuthentication




router=APIRouter()
limiter = Limiter(key_func=get_remote_address)


add_pagination(router)





async def notBeinglock(
        request:Request,
        db:AsyncSession=Depends(get_db)
):
    
    from models.account import BlockUser
    user_ip=request.client.host

    c=await db.execute(
        select(
            BlockUser
        ).where(
            BlockUser.user_ip==user_ip
        ).limit(1)
    )

    c_sc=c.scalar()

    if c_sc:
        raise HTTPException(detail="user already block to sending request",status_code=404)
    
    return user_ip









from sqlalchemy.orm import joinedload,subqueryload,lazyload
from sqlalchemy.orm import contains_eager
from sqlalchemy.orm import aliased




def get_children(obj,include_self=True):


    r=[]
    for i in obj:
         if i.child:
            # print(i.child)
            for j in i.child:
                if j.child:
                    print(j.child)
            #         # pass
            #         print(j.id)
                    r.append(get_children(j))
    

    # return r

    # if obj.child:
    #     for j in obj.child:
    #         if j.child:
    #             get_children(j)

    #             print(j)
    


@router.get(
    path="/",
    description="all category in main page",
    summary="all categories",
    response_class=JSONResponse,

    # response_model=list[CategorySchema],

    response_model_exclude_unset=True,
    # response_model_include=["id"],
    response_description="All category in home page",

    deprecated=False,
    include_in_schema=True,
    dependencies=[
        Depends(notBeinglock)
    ]
)


async def  getAllCategories(
    request:Request,
    
    db:AsyncSession=Depends(get_db),
):  
    from models.category import Category
    # a=await db.execute()
    
    
    
    # c=await db.execute(
    #     select(Category)
    #         .where(
    #             Category.is_active == True,
    #                Category.parent_id.is_(None),
    #             #    Category.child.property.
    #         ).options(
    #             lazyload(Category.child)
    #         )
    #     )
    
    
# 
    # nodealias = aliased(Category)

    # c=await db.execute(
    #     select(Category)
    #     .where(Category.is_active == True)
    #     .join(Category.parent.of_type(nodealias))
    #     .where(nodealias.is_active==True)
    # )


    c=await db.execute(
        select(Category).where(
            Category.parent_id.is_(None),
            Category.is_active==True
        )
    )
    c_sc=c.unique().scalars().all()

    da=getData(c_sc)
    return da

    




def foryieldData(data,page):

    for i in data:
        # print(i.id)
        yield{
            "in_cart":i[1],
            "name":i[0].name,
            "id":i[0].id,
            'page':page
        }




@router.get(
        path="/category/{slug}/",
        description="each category products",
        summary="each profucts",
        response_class=JSONResponse,
        response_description="each product",
        deprecated=False,
        include_in_schema=True,
        dependencies=[
            Depends(notBeinglock)
        ],
    # response_model=list[ProductSchem]|list[dict]
    response_model=list[dict]
) 
async def eachCategory(

    request:Request,
    slug:str=Path(
        description="each category slug",
        title="category slug title",
        min_length=5,
    ),
    only_off:bool|None=Query(default=False),
    min_price:float=Query(default=10),
    max_price:float=Query(default=1000000),
    sort:str=Query(default='created'),
    p_type:list[str]=Query(default=''),
    page:int=Query(default=1),
    db:AsyncSession=Depends(get_db),
):
    from models.category import Category,Product
    from models.cart import Cart,CartItem
    from models.account import User


    i=await convert_str_to_uuid(slug)

    c=await db.execute(
        select(Category).where(
            Category.is_active==True,
            Category.id==i
        ).limit(1)
    )

    c_sc=c.scalar()
   

    if not c_sc:
        raise HTTPException(detail='Category not found',status_code=404)
 
    filters=(
        and_(Product.is_active==True) if only_off  else and_(),
        and_(Product.price>float(min_price)) if min_price else and_(),
        # and_(Product.price<max_price) if max_price else and_(),
        and_(Product.product_type.in_(p_type)) if p_type else and_(),

    )

    test_user_logged_in=await IsuserLoggedIn(request,db)
    if not test_user_logged_in:


        p=await db.execute(
            select(
                Product
            ).where(
                Product.category_id==c_sc.id,
                # Product.is_active==True
            ).where(
                *filters
            ).order_by(
                text(sort)
            )
        )

        p_sc=p.scalars().unique().all()
    
        return p_sc
    

    else:

        user_id=test_user_logged_in[1]
        user=await db.execute(
            select(User).where(User.id==await convert_str_to_uuid(user_id)).limit(1)
        )
        user_sc=user.scalar()
        if not user_sc:

            p=await db.execute(
            select(
                Product
            ).where(
                Product.category_id==c_sc.id,
                # Product.is_active==True
            ).where(
                *filters
            ).order_by(
                text(sort)
            )
        )

            p_sc=p.scalars().unique().all()
        
            return p_sc

        else:

            c=await db.execute(
                select(CartItem).where(
                    # CartItem.product_id==p_sc.id,
                    CartItem.is_paid==False,
                    CartItem.user_id==await convert_str_to_uuid("b084484e3fe24d7893b13f782f6c8409")
                    )
            )

            cart_sc=c.scalars().all()

            ids=[]
            for i in cart_sc:
                ids.append(i.product_id)

            ps=await db.execute(
                select(
                    Product,
                    case(
                        (
                            Product.id.in_(ids),True
                        ),else_=False
                    )
                ).where(Product.category_id==c_sc.id)
                .where(
                        *filters
                    ).where(
                        # Product.id.not_in(ids)
                    ).order_by(
                        text(sort)
                    ).limit(10).offset(page)
            )

        # for i in ps:
        #     print(i[0].id)

            d=foryieldData(ps,page)

            return d

                

            




def eachProductIfUserLoggedIn(data):
    yield{
            "in_cart":data[1],
            "name":data[0].name,
            "id":data[0].id,
        }







@router.get(
    path="/{id}/",
    description="each product detail",
    response_class=JSONResponse,
    deprecated=False,
    include_in_schema=True,
    response_description='each product with id',
    dependencies=[
            Depends(notBeinglock)
        ],
    response_model=ProductSchem2Detail,
    summary="each product"
)

async def eachProduct(
    request:Request,
    id:uuid.UUID=Path(
        description="product id",
        title="id",
    ),
    db:AsyncSession=Depends(get_db),
):
    
    from models.category import Product,ProductReview
    from models.cart import CartItem


    test_user_logged_in=await IsuserLoggedIn(request,db)
    if not test_user_logged_in:

        p=await db.execute(
            select(
                Product
            ).where(Product.id==id,
                    Product.is_active==True
            )
        )
        p_sc=p.scalar()
        return p_sc
    
    else:

        user_id=await convert_str_to_uuid(str(test_user_logged_in[1]))
        user_carts=await db.execute(
            select(CartItem).where(CartItem.is_paid==False,
                                   CartItem.user_id==user_id)
        )

        c_sc=user_carts.scalars().unique().all()
        ids=[]
        for i in c_sc:
            ids.append(i.product_id)

        p=await db.execute(
            select(

                Product
                ,
                case(
                    (
                        Product.id._in(ids),True
                    ),else_=False
                )
                   ).where(
                    Product.id==id,
                    Product.is_active==True
                )
        )

        p_data=eachProductIfUserLoggedIn(p)

        return p_data
    

        











@router.get(
        path="/review/{id}/",
        description="each product reviews",
        response_class=JSONResponse,
        response_description="each product reviews",
        summary="reviews",
        include_in_schema=True,
        deprecated=False,
        response_model=Page[ReviewProductSchema]
)
async def reviews(
    request:Request,
    params:Params=Depends(),
    id:uuid.UUID=Path(
        description="product id",
        title="id",
    ),
    db:AsyncSession=Depends(get_db),
):
    

    from models.category import Product,ProductReview
    params.size=1
    p=await db.execute(
        select(
            Product
        ).where(Product.id==id,
                Product.is_active==True
        )
    )
    p_sc=p.scalar()

    r=await db.execute(
        select(ProductReview).where(
            ProductReview.product_id==p_sc.id
        )
    )

    r_sc=r.scalars().all()

    return paginate(r_sc,params=params)










@router.get(
    path="/avg/{id}/",
    description="each product avg",
    response_class=JSONResponse,
    response_description="each product avg",
    summary="avg",
    include_in_schema=True,
    deprecated=False,
    response_model=dict
)


    
async def eachProductAvg(
    request:Request,
    id:uuid.UUID=Path(
        description="product id",
        title="id",
    ),
    db:AsyncSession=Depends(get_db),
):
    
    from models.category import Product,ProductReview

    p=await db.execute(
        select(
            func.avg(ProductReview.quality_rate).label("rate")
        ).where(ProductReview.product_id==id,
                # Product.is_active==True
        )
    )
    p_sc=p.scalar()

    return p_sc







@router.get(
    path="/related/{id}/",
    description="each product related",
    response_class=JSONResponse,
    response_description="each product related",
    summary="related",
    include_in_schema=True,
    deprecated=False,
    response_model=dict
)

async def relatedProducts(
    request:Request,
    id:uuid.UUID=Path(
        description="product id",
        title="id",
    ),
    db:AsyncSession=Depends(get_db),
):
    
    from models.category import Product,ProductReview

    p=await db.execute(
        select(
           Product
        ).where(Product.id==id,
                Product.is_active==True
        )
    )

    p_sc=p.scalar()

    r=await db.execute(
        select(Product).where(
            Product.id!=p_sc.id,
        
        ).where(
            or_(
                Product.english_name.icontains(p_sc.english_name),
                Product.name.icontains(p_sc.name),
                Product.product_type==p_sc.product_type
            )
        )

    )


    















@router.get(
    path="/test",

) 

async def addData(
    request:Request,
    db:AsyncSession=Depends(get_db)
):
    from models.category import Category,Product,ProductImage,ProductReview
    from models.account import User
    from models.cart import Cart,CartItem
    # c=Category.__table__.delete().where(Category.is_active==False)
    # await db.execute(c)
    # await db.commit()

    # new_u=User(
    #     phone="09178588832",is_active=True
    # )

    # db.add(new_u)
    # await db.commit()
    # await db.refresh(new_u)




    c1=Category(
        name="category number 1",
        is_active=True,
        description="hello this is descriptipon"
    )
    db.add(c1)
    await db.commit()
    await db.refresh(c1)


    c=await db.execute(
        select(Category).where(Category.name=="category number 1").limit(1)
    )
    c_sc=c.scalar()

    for i in range(40,45):
        new_child=Category(
            name=f"number category{i}",
            is_active=True if i%2==0 else False,
            parent_id=c_sc.id
        )
        db.add(new_child)
        await db.commit()
        await db.refresh(new_child)



    for i in range(12,15):
        p=Product(
            name=f"pills number {i}",
            title=f"title number {i}",
            english_name=f"name is {i}",
            price=(i+10)*10000,
            is_active=True,
            warning=f"warning {i}",
            numbers_in_each_box=(i+1)*10,
            category_id=c1.id,
        )
        db.add(p)
        await db.commit()
        await db.refresh(p)


    p=await db.execute(
        select(Product).where(Product.name=="pills number 12").limit(1)
    )
    u=await db.execute(
        select(User).where(User.phone=="09178588832").limit(1)
    )
    u_sc=u.scalar()
    
    p_s=p.scalar()

    new_cart=Cart(
        cart_id_unique=str(u_sc.id),
        is_active=True
    )

    db.add(new_cart)
    await db.commit()
    await db.refresh(new_cart)

    new_c=await db.execute(
        select(Cart).where(Cart.cart_id_unique==str(u_sc.id)).limit(1)
    )

    new_cart=new_c.scalar()

    c1=CartItem(
        user_id=u_sc.id,
        is_paid=False,
        cart_id=new_cart.id,
        product_id=p_s.id,
        quantity=2
    )

    db.add(c1)
    await db.commit()
    await db.refresh(c1)



    p2=await db.execute(
        select(Product).where(Product.name=="pills number 13").limit(1)
    )
    p2_sc=p2.scalar()

    c2=CartItem(
        user_id=u_sc.id,
        is_paid=False,
        cart_id=new_cart.id,
        product_id=p2_sc.id,
        quantity=1
    )

    db.add(c2)
    await db.commit()
    await db.refresh(c2)
    

    
    p3=await db.execute(
        select(Product).where(Product.name=="pills number 14").limit(1)
    )
    p3_sc=p3.scalar()

    c3=CartItem(
        user_id=u_sc.id,
        is_paid=False,
        cart_id=new_cart.id,
        product_id=p3_sc.id,
        quantity=3
    )


    try:
        p_s.favorites.append(u_sc)
        await db.commit()
    except Exception as e:
        print("eeeeeeeeeeeeee")
        print(e)
    for i in range(1,5):
        new_r=ProductReview(
            quality_rate=i,
            user_id=u_sc.id,
            text=f"title  {i}",
            product_id=p_s.id
        )
        db.add(new_r)
        await db.commit()
    #     await db.refresh(new_r)



    return {
        "okay":True
    }









@router.get(
    path="/fav-category"
)

async def favoraotes_ctegory(
    request:Request,
    db:AsyncSession=Depends(get_db)
):
    
    from models.category import Product,Category,ProductReview

    p=await db.execute(
        select(Product.category_id).join(ProductReview).group_by(
                ProductReview.id
            ).order_by(func.count()).limit(2)
    )

    a=p.scalars().unique().all()
    print(a)

    c=await db.execute(
        select(
            Category
        ).where(
            Category.id.in_(a)
        )
    )

    

    return {
        "okay":True,
        "a":c.scalars().unique().all()
    }



@router.post(
    path="/add-favorate/{id}",
    description="add product to favorates",
    response_description="add new product to favoraies",
    summary="add product saved",
    response_class=JSONResponse,
    include_in_schema=True,
    deprecated=False,
    response_model=dict
)

async def add_favoraote(
    request:Request,
    id:uuid.UUID=Path(
        title="id",
        description="id of product",
    ),
    auth_:dict=Depends(customAuthentication),
    db:AsyncSession=Depends(get_db),
):
    
    from models.category import Category,Product
    from models.account import User,Token

    c=await db.execute(
        select(Token).where(Token.key==auth_.get("token"))
    )

    c_sc=c.scalar()

    p=await db.execute(
        select(Product).where(
                            Product.is_active==True,
                            Product.id==id
                            ).limit(1)
    )

    p_sc=p.scalar()


    founded_user=await db.execute(
        select(User).where(User.id==c_sc.user_id).limit(1)
    )
    founded_user_sc=founded_user.scalar()


    if not p_sc:
        raise HTTPException("product not found",status_code=404)
    
    added=False
    if founded_user_sc in p_sc.favorites:
        p_sc.favorites.remove(founded_user_sc)
        await db.commit()
        await db.refresh(p_sc)

        added=False
    else:

        p_sc.favorites.add(founded_user_sc)
        await db.commit()
        await db.refresh(p_sc)
        added=True

    
    return {
        "added":added
    }



    
