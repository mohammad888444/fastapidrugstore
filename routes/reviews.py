from fastapi import (
    FastAPI,APIRouter,Depends,
    Header,HTTPException,
    Cookie,Query,Request,Response,
    Path,File,UploadFile,Form,BackgroundTasks
)
from fastapi.responses import JSONResponse,HTMLResponse,RedirectResponse

from utils import (
    generate_hash_password,
    createCustomHeader,makeUserToken,
    send_otp,creatTokenForOtp,
    getData,convert_str_to_uuid
)
import time
from datetime import datetime,timedelta
import uuid
 

from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy import select,delete,update,func,and_,desc,asc,text,case,or_


from database import get_db
from schema.user import UserRegister,UserLogin


from slowapi.errors import RateLimitExceeded
from slowapi import Limiter, _rate_limit_exceeded_handler
from slowapi.util import get_remote_address

from decorator import notLoginRequired,notTokenInHeader,IsuserLoggedIn
from fastapi_pagination import Page, add_pagination, paginate,Params
from schema.category import SecondCategorySchema,ThirdCategorySchema,CategorySchema,ProductSchem,ProductSchem2Detail,ReviewProductSchema
from routes.account import customAuthentication
from schema.review import ReviewMakeSchema






router=APIRouter()
limiter = Limiter(key_func=get_remote_address)


add_pagination(router)






async def userAllowedToReview(
    id:uuid.UUID=Path(),
    auth_:dict=Depends(customAuthentication),
    db:AsyncSession=Depends(get_db),
):
    
    from models.category import ProductReview,Product
    from models.account import Token,User
    from models.orders import OrderProduct

    p=await db.execute(
        select(Product).where(
            Product.is_active==True,
            Product.id==id
            )
    )
    
    p_sc=p.scalar()

    if not p_sc:
        raise HTTPException("product not found",status_code=404)
    
    c=await db.execute(
        select(Token).where(Token.key==auth_.get("token"))
    )

    c_sc=c.scalar()

    o=await db.execute(
        select(OrderProduct).where(
                OrderProduct.user_id==c_sc.user_id,
                OrderProduct.product_id==p_sc.id
            )
    )

    o_sc=o.scalar()

    if not o_sc:
        raise HTTPException("order product not found",status_code=404)
    
    r=await db.execute(
        select(ProductReview).where(
            ProductReview.product_id==p_sc.id,
            ProductReview.user_id==c_sc.user_id,
            ProductReview.quality_rate.is_not(None)
        )
    )

    r_sc=r.scalar()
    if  not r_sc:
        return {
            "rate":True,
            "p_id":p_sc.id
        }
    else:
        return {
            "rate":False,
            "p_id":p_sc.id

        }








@router.post(
    path="/add/{id}",
    description="add new review",
    summary="add review for product",
    response_description="add new review",
    response_class=JSONResponse,
    include_in_schema=True,
    deprecated=False,
    response_model=dict
)


async def addReview(
    request:Request,
    data:ReviewMakeSchema,
    id:uuid.UUID=Path(
        title="id",
        description="id of product",
    ),
    auth_:dict=Depends(customAuthentication),
    db:AsyncSession=Depends(get_db),
    before:dict=Depends(userAllowedToReview)
):
    
    from models.category import ProductReview
    from models.account import Token,User

    c=await db.execute(
        select(Token).where(Token.key==auth_.get("token"))
    )

    c_sc=c.scalar()

    if not before.get("rate"):
        rate=data.rate
        text=data.text
        new_review=ProductReview(
            quality_rate=rate,
            text=text,
            product_id=before.get("p_id"),
            user_id=c_sc.user_id
        )

        db.add(new_review)
        await db.commit()
        await db.refresh(new_review)
        
        return {
            "id":new_review.id,
            "text":new_review.text,
            "rate":new_review.quality_rate
        }
    
    
    


