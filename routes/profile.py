from fastapi import (
    FastAPI,APIRouter,Depends,
    Header,HTTPException,
    Cookie,Query,Request,Response,
    Path,File,UploadFile,Form,BackgroundTasks
)
from fastapi.responses import JSONResponse,HTMLResponse,RedirectResponse

from utils import (
    generate_hash_password,
    createCustomHeader,makeUserToken,
    send_otp,creatTokenForOtp,
    getData,convert_str_to_uuid
)
import time
from datetime import datetime,timedelta
import uuid
 

from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy import select,delete,update,func,and_,desc,asc,text,case,or_


from database import get_db
from schema.user import UserRegister,UserLogin


from slowapi.errors import RateLimitExceeded
from slowapi import Limiter, _rate_limit_exceeded_handler
from slowapi.util import get_remote_address

from decorator import notLoginRequired,notTokenInHeader,IsuserLoggedIn
from fastapi_pagination import Page, add_pagination, paginate,Params
from schema.category import SecondCategorySchema,ThirdCategorySchema,CategorySchema,ProductSchem,ProductSchem2Detail,ReviewProductSchema
from routes.account import customAuthentication
from schema.review import ReviewMakeSchema
from schema.order import OrdersListSchema






router=APIRouter()
limiter = Limiter(key_func=get_remote_address)


add_pagination(router)




@router.get(
    path="/orders",
    description="my orders",
    response_description="my order in profile",
    summary="orders in profile",
    response_class=JSONResponse,
    include_in_schema=True,
    deprecated=False,
    response_model=Page[OrdersListSchema]
)

async def myOrders(
    request:Request,
    params:Params=Depends(),
    db:AsyncSession=Depends(get_db),
    auth_:dict=Depends(customAuthentication),
):
    params.size=1

    from models.account import Token,User
    from models.orders import Order

    c=await db.execute(
        select(Token).where(Token.key==auth_.get("token"))
    )

    c_sc=c.scalar()

    o=await db.execute(
        select(Order).where(
                    Order.user_id==c_sc.user_id
                            )
    )

    o_sc=o.scalars().unique().all()

    return paginate(o_sc,params=params)








