from pydantic import BaseModel,EmailStr,Field,validator,computed_field
import uuid
from datetime import datetime


class CategorySchema(BaseModel):

    id:uuid.UUID
    name:str
    # child:list|None
    # value: str|uuid.UUID

    @computed_field
    @property
    def value(self) -> str:
        return str(self.id)[:4]
    


    
    

 

class SecondCategorySchema(CategorySchema):
    child:list[CategorySchema]
    


class ThirdCategorySchema(SecondCategorySchema):
    child:list[SecondCategorySchema]




class ProductImageSchema(BaseModel):
    id:uuid.UUID
    image:str|None=Field(
        default=None
    )



class ProductReviewSchema(BaseModel):
    id:uuid.UUID
    text:str|None
    quality_rate:float|None
    user_id:uuid.UUID
    product_id:uuid.UUID




class UserSchem(BaseModel):

    id:uuid.UUID


class ProductSchem(BaseModel):

    id:uuid.UUID
    category_id:uuid.UUID
    favorites:list[UserSchem]|None
    images:list[ProductImageSchema]|None
    product_reviews:list[ProductReviewSchema]|None
    is_active:bool
    title:str
    name:str
    english_name:str
    price:float
    test:str
    # in_cart:bool=Field(default=False)

    class Config : 
        from_attributes = True

    
    




class ProductSchem2Detail(BaseModel):

    id:uuid.UUID
    category_id:uuid.UUID
    # favorites:list[UserSchem]|None
    images:list[ProductImageSchema]|None
    # product_reviews:list[ProductReviewSchema]|None
    is_active:bool
    title:str
    name:str
    english_name:str
    price:float
    test:str
    

    class Config : 
        from_attributes = True

    
    





class ReviewProductSchema(BaseModel):

    quality_rate:float|None
    user_id:uuid.UUID
    product_id:uuid.UUID
    text:str|None
    created:datetime