from pydantic import BaseModel,validator,EmailStr,Field
from fastapi import HTTPException
import uuid
from datetime import datetime


class UpdateOrderSchema(BaseModel):

    name:str=Field()
    email:EmailStr=Field()

    address:str=Field()





class OrdersListSchema(UpdateOrderSchema):
    custom_order_id:str
    user_id:uuid.UUID
    is_paid:bool
    used_copon:bool
    pay_link:str
    amount:float
    amount_with_copon:float
    created:datetime