from pydantic import BaseModel,validator,EmailStr,Field
from fastapi import HTTPException

class CoponSchema(BaseModel):

    code:str=Field(
        title="code of copon",
        description='copon',
        max_length=8
    )



    @validator("code")
    def validate_code(cls,value:str):
        if len(value)!=8:
            raise HTTPException(detail="code is not correct : length",status_code=404)
        return value