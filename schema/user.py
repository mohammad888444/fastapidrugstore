from pydantic import BaseModel,Field,EmailStr,validator
from fastapi import HTTPException


class UserRegister(BaseModel):

    phone:str=Field(
        title="phone",
        description="phone",
        alias="phone"
    )
    # email:EmailStr=Field(
    #     title="email",
    #     description="email"
    # )

    # password1:str
    # password2:str

    @validator("phone")
    def validate_phone(cls,value:str):
        if 10<=len(value)<=11:
            if value.startswith("0"):
                return value
            raise HTTPException(detail="phone start with 0")
        raise HTTPException(detail="phone length error",status_code=404)





class UserLogin(BaseModel):

    code:str

    @validator("code")
    def validate_code(cls,value):
        if len(value)!=5:
            raise HTTPException(detail="code lenght is not valid")
        return value