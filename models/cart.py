from sqlalchemy import Column,Integer,String,ForeignKey,DateTime,select,Enum,UUID,Boolean,Text,Float
from sqlalchemy.orm import relationship,validates
from database import Base
import uuid
from enum import Enum as EnumBase
from datetime import datetime
from fastapi import HTTPException
import re

from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method
from sqlalchemy import select

# from sqlalchemy.orm import object_session
from sqlalchemy.ext.asyncio.session import async_object_session
from sqlalchemy import select


class Cart(Base):

    __tablename__="cart"

    id=Column(
        "id",
        UUID,
        index=True,
        primary_key=True,
        unique=True,
        default=uuid.uuid4
    )

    cart_id_unique=Column(
        String,unique=True,index=True
    )

    is_active=Column("is_active",Boolean,default=True,index=True)

    items=relationship("CartItem",backref="cart",lazy="selectin")

    def __str__(self):
        return str(self.cart_id_unique)
    


class CartItem(Base):

    __tablename__="cart_item"

    id=Column(
        "id",
        UUID,
        index=True,
        primary_key=True,
        unique=True,
        default=uuid.uuid4
    )

    user_id=Column('user_id',UUID,ForeignKey("user.id"),index=True)
    cart_id=Column('cart_id',UUID,ForeignKey("cart.id"),index=True)
    product_id=Column('product_id',UUID,ForeignKey("product.id"),index=True)
    is_paid=Column(Boolean,default=False)
    quantity=Column(Integer,default=1,index=True)

    



    def __str__(self):
        return str(self.user_id)
    