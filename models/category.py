from sqlalchemy import Column,Integer,String,ForeignKey,DateTime,select,Enum,UUID,Boolean,Text,Float
from sqlalchemy.orm import relationship,validates
from database import Base
import uuid
from enum import Enum as EnumBase
from datetime import datetime
from fastapi import HTTPException
import re

# from slugify import slugify
import asyncio
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method
from sqlalchemy import select

from sqlalchemy.orm import object_session
from sqlalchemy.ext.asyncio.session import async_object_session
from sqlalchemy import select


class Category(Base):

    __tablename__="category"

    id=Column(
        "id",
        UUID,
        index=True,
        primary_key=True,
        unique=True,
        default=uuid.uuid4
    )

    name=Column("name",String(150),index=True,unique=True)
    is_active=Column("is_active",Boolean,default=False,index=True)
    short_id=Column("short_id",String(100),index=True,unique=True,nullable=True)
    slug=Column("slug",String,index=True,nullable=True)
    description=Column("description",Text,index=True,nullable=True,)
    image=Column("image",String,index=True,nullable=True)

    parent_id=Column("parent_id",UUID,ForeignKey("category.id"),index=True)
    parent = relationship("Category", back_populates="child", remote_side=[id])


    child=relationship("Category",lazy="joined",join_depth=3)
    products=relationship("Product",lazy="selectin",back_populates="category")



    created=Column(DateTime,default=datetime.utcnow,index=True)
    updated=Column(DateTime,default=datetime.utcnow,index=True)

    @hybrid_property
    def length(self):
        return len(self.name)
    
    
    def __str__(self):
        return str(self.name)
    
    @hybrid_property
    def value(self):
        return self.parent_id

    @hybrid_property
    def activated(self):
        return select(
            self.child
        ).filter_by(is_active=True)

    @activated.expression
    def activated_in_expression(cls):
       return  select(
            cls.child
        ).filter_by(is_active=True)
    
    # @property
    def get_children(self):
          children = list()
          children.append(self)
          for child in self.child:
              children.extend(children.get_children())
          return children
    
    # @property
    async def author(self):
        return async_object_session(self)
            
    

class ProductType(EnumBase):

    pill="1"
    capsul="2"


    
class Vitimins(EnumBase):
    b="vitamin_b"
    c="vitamin_c"
    d="vitamin_d"
    a="vitamin_a"
    k="vitamin_k"
    # ="vitamin_k"











class Product(Base):

    __tablename__="product"
    
    id=Column(
        "id",
        UUID,
        index=True,
        primary_key=True,
        unique=True,
        default=uuid.uuid4
    )

    name=Column("name",String(150),index=True,unique=True)
    title=Column("title",String(150),index=True,unique=True)
    english_name=Column("english_name",String(150),index=True,unique=True)


    is_active=Column("is_active",Boolean,default=False,index=True)
    short_id=Column("short_id",String(100),index=True,unique=True,nullable=True)
    expire_time=Column(String,default="1",index=True)
    product_type=Column(Enum(ProductType),index=True,default="pill")
    vitamin_group=Column(Enum(Vitimins),index=True,default=Vitimins.b)
    warning=Column(Text,index=True)

    

    price=Column("price",Float,index=True)
    price_with_off=Column("price_with_off",Float,index=True)
    numbers_in_each_box=Column(Integer,default=20,index=True)

    order_products=relationship("OrderProduct",back_populates="product",lazy="selectin")

    category_id=Column("category_id",UUID,ForeignKey("category.id"),index=True)
    category=relationship("Category",back_populates="products",lazy="selectin")
    favorites=relationship("User",secondary="user_favourates",lazy="selectin",backref="products")
    images=relationship("ProductImage",secondary="product_multimage_manytomany",lazy="selectin",backref="products")
    product_reviews=relationship("ProductReview",back_populates="product",lazy="selectin")
    items=relationship("CartItem",backref="product",lazy="selectin")





    created=Column(DateTime,default=datetime.utcnow,index=True)
    updated=Column(DateTime,default=datetime.utcnow,index=True)


    # @hybrid_property
    # def test(self):
    #     return str(len(self.name))
    

    # @property
    # def in_cart(self):
     
    #     loop = asyncio.get_running_loop()
    #     try:
    #         res=asyncio.run(self.ff())
    #         print(res)
    #     except Exception as e:
    #         print(e)
    #         return False



    #     return res
    

    # async def ff(self):
    #     from models.cart import Cart,CartItem

    #     a=select(CartItem).where(CartItem.product_id==self.id).limit(1)

    #     result = await async_object_session(self).execute(a)
    #     s=result.scalar()

    #     if s:
    #         print("YEYEYEYSYSYSYSYSYS",s.id)
    #         return True
    #     else:
    #         print("NOOONOONNNNN",s.id)
    #         return False




    

class ProductReview(Base):

    __tablename__="product_review"

    id=Column(
        "id",
        UUID,
        index=True,
        primary_key=True,
        unique=True,
        default=uuid.uuid4
    )

    quality_rate=Column(Float,default=5)
    text=Column("text"
    ,Text,index=True)

    user_id=Column("user_id",UUID,ForeignKey("user.id"),index=True)
    product_id=Column("product_id",UUID,ForeignKey("product.id"),index=True)


    user=relationship("User",back_populates="product_reviews")
    product=relationship("Product",back_populates="product_reviews")




    created=Column(DateTime,default=datetime.utcnow,index=True)
    updated=Column(DateTime,default=datetime.utcnow,index=True)

    @validates("quality_rate")
    def validate_quality_rate(self,key,value):
        if not 0<=value<=5:
            raise HTTPException(detail="not value appoprtie",status_code=404)
        return value
    





class ProductImage(Base):

    __tablename__="product_image"

    id=Column(
        "id",Integer,primary_key=True,
        unique=True,
        index=True,
        autoincrement=True
    )
    image=Column("image",String,index=True,nullable=True)







class ProductMultImageManyToMany(Base):

    __tablename__="product_multimage_manytomany"

    id=Column(
        "id",Integer,primary_key=True,
        unique=True,
        index=True,
        autoincrement=True
    )


    product_id=Column("product_id",UUID,ForeignKey("product.id"),index=True)
    product_image_id=Column("product_image_id",UUID,ForeignKey("product_image.id"),index=True)




    

class UserFavaorates(Base):

    __tablename__="user_favourates"

    id=Column(
        "id",Integer,primary_key=True,
        unique=True,
        index=True,
        autoincrement=True
    )

    user_id=Column("user_id",UUID,ForeignKey("user.id"),index=True)
    product_id=Column("product_id",UUID,ForeignKey("product.id"),index=True)



 