from sqlalchemy import Column,Integer,String,ForeignKey,DateTime,select,Enum,UUID,Boolean
from sqlalchemy.orm import relationship,validates
from database import Base
import uuid
from enum import Enum as EnumBase
from datetime import datetime
from fastapi import HTTPException
import re






def check_password_validation(password):

    flag = None

    while True:
        if (len(password)<=8):
            flag ="length"
            break
        elif not re.search("[a-z]", password):
            flag = "a-z"
            break
        elif not re.search("[A-Z]", password):
            flag = "A-Z"
            break
        elif not re.search("[0-9]", password):
            flag = "0-9"
            break
        elif not re.search("[_@$]" , password):
            flag = "special"
            break
        # elif re.search("\s" , password):
        #     flag = -1
        #     break
        else:
            flag = None
            print("Valid Password")
            break
        
    if not flag:
        return None
    return False,flag
 




class User(Base):
    __tablename__="user"
    id=Column(
        "id",
        UUID,
        index=True,
        primary_key=True,
        unique=True,
        default=uuid.uuid4
    )

    email=Column("email",String(100),index=True,)
    username=Column("username",String(100),index=True,)
    phone=Column("phone",String(100),index=True,unique=True)
    is_active=Column("is_active",Boolean,default=False,index=True)
    password=Column("password",String(300),index=True,)
    is_admin=Column(Boolean,default=False,index=True)
    is_superuser=Column(Boolean,default=False,index=True)
    user_ip=Column(String,index=True,)
    
    block_user=relationship("BlockUser",back_populates="user",uselist=False,lazy="selectin")
    token=relationship("Token",back_populates="user",uselist=False,lazy="selectin")
    secret_key=relationship("CustomizedSecretKey",back_populates="user",uselist=False,lazy="selectin")
    user_otp=relationship("UserOtp",back_populates="user",uselist=False,lazy="selectin")
    items=relationship("CartItem",backref="user",lazy="selectin")
    orders=relationship("Order",back_populates="user",lazy="selectin")
    order_products=relationship("OrderProduct",back_populates="user",lazy="selectin")

    payments=relationship("Payment",back_populates="user",lazy="selectin")
    product_reviews=relationship("ProductReview",back_populates="user",lazy="selectin")
    



    
    created=Column(DateTime,default=datetime.utcnow,index=True)
    updated=Column(DateTime,default=datetime.utcnow,index=True)


    def __str__(self):
        return str(self.email)
    




    @validates("password")
    def validate_password(self,key,value):
        
        c=check_password_validation(value)
        if c:
            match c[1]:
                
                case "length":
                    raise HTTPException("password length has error",status_code=404)
                case "a-z":
                    raise HTTPException("password should contains lower case characters",status_code=404)
                case "A-Z":
                    raise HTTPException("password should contains upper case characters",status_code=404)
                case "0-9":
                    raise HTTPException("password should contains numbers",status_code=404 )       
                case "special":
                    raise HTTPException("password should contains at least one special characters",status_code=404)
                case _:
                    raise HTTPException("password is not correct",status_code=404)
        else:
            return value



    


class BlockUser(Base):  

    __tablename__="blockuser"
    id=Column(
        "id",
        UUID,
        index=True,
        primary_key=True,
        unique=True,
        default=uuid.uuid4
    )
    
    user_ip=Column(String,index=True,)
    user_id=Column("user_id",UUID,ForeignKey("user.id"),index=True)
    user=relationship("User",back_populates="block_user")

    created=Column(DateTime,default=datetime.utcnow,index=True)
    updated=Column(DateTime,default=datetime.utcnow,index=True)











class Token(Base):
    __tablename__='token'

    id=Column("id",UUID,default=uuid.uuid4,unique=True,index=True,primary_key=True,)
    user_id=Column("user_id",UUID,ForeignKey("user.id"),index=True)
    user=relationship("User",back_populates="token")
    key=Column(String(128),index=True,)
    created=Column(DateTime,default=datetime.utcnow,index=True)

    def __str__(self):
        return str(self.user_id)
    










class CustomizedSecretKey(Base):
    __tablename__="secret_key"

    id=Column("id",Integer,index=True,primary_key=True,autoincrement=True,unique=True)
    key=Column(String(128),index=True)
    user_ip=Column(String,index=True,)

    user_id=Column("user_id",UUID,ForeignKey("user.id"),index=True)

    user=relationship("User",back_populates="secret_key")

    created=Column(DateTime,default=datetime.utcnow,index=True)


    def __str__(self):
        return str(self.user)



    



class UserOtp(Base):

    __tablename__="user_otp"
    id=Column("id",Integer,index=True,primary_key=True,autoincrement=True,unique=True)
    user_id=Column("user_id",UUID,ForeignKey("user.id"),index=True)
    user=relationship("User",back_populates="user_otp")
    otp=Column(String(6),nullable=False,index=True)
    expire=Column(DateTime,nullable=False,index=True)


    def __str__(self):
        return str(self.otp)





