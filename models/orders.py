from sqlalchemy import Column,Integer,String,ForeignKey,DateTime,select,Enum,UUID,Boolean,Float
from sqlalchemy.orm import relationship,validates
from database import Base
import uuid
from enum import Enum as EnumBase
from datetime import datetime
from fastapi import HTTPException
import re




class Copon(Base):

    __tablename__="copon"

    id=Column(
        "id",
        UUID,
        index=True,
        primary_key=True,
        unique=True,
        default=uuid.uuid4
    )

    code=Column(String(10),index=True)
    percent=Column(Integer,index=True,default=10)
    numbers=Column(Integer,default=10,index=True)
    expire=Column(DateTime,default=datetime.utcnow(),index=True)

    def __str__(self):
        return str(self.code)
    







class Payment(Base):    

    __tablename__="payment"

    id=Column(
        "id",
        UUID,
        index=True,
        primary_key=True,
        unique=True,
        default=uuid.uuid4
    )

    user_id=Column("user_id",UUID,ForeignKey("user.id"),index=True)
    user=relationship("User",back_populates="payments",lazy="selectin")
    is_paid=Column(Boolean,default=False,index=True)
    bank_status=Column(String(45),index=True)
    tracking_code=Column(String(65),index=True)
    bank_name=Column(String(45),index=True)
    order=relationship("Order",back_populates="payment",lazy="selectin",uselist=False)
    
    created=Column(DateTime,default=datetime.utcnow,index=True)
    updated=Column(DateTime,default=datetime.utcnow,index=True)


class Order(Base):

    __tablename__="order"

    id=Column(
        "id",
        UUID,
        index=True,
        primary_key=True,
        unique=True,
        default=uuid.uuid4
    )

    custom_order_id=Column(String(40),unique=True,index=True,)

    user_id=Column("user_id",UUID,ForeignKey("user.id"),index=True)
    user=relationship("User",back_populates="orders",lazy="selectin")

    cartitems=relationship("CartItem",secondary="order_cartitems",backref="orders",lazy="selectin")
    order_products=relationship("OrderProduct",back_populates="order",lazy="selectin")
    payment=relationship("Payment",back_populates="order",lazy="selectin")
    payment_id=Column("payment_id",UUID,ForeignKey("payment.id"),index=True)

    name=Column(String(40),index=True)
    email=Column(String(40),index=True)
    address=Column(String(100),index=True)
    used_copon=Column(Boolean,default=False,index=True)
    is_paid=Column(Boolean,default=False,index=True)
    pay_link=Column(String,index=True)
    authority=Column(String,index=True)

    amount=Column(Float,index=True)
    amount_with_copon=Column(Float,index=True)
    
    created=Column(DateTime,default=datetime.utcnow,index=True)
    updated=Column(DateTime,default=datetime.utcnow,index=True)




    def __str__(self):
        return str(self.name)
    


class OrderProduct(Base):

    __tablename__="order_product"

    id=Column(
        "id",
        UUID,
        index=True,
        primary_key=True,
        unique=True,
        default=uuid.uuid4
    )


    user_id=Column("user_id",UUID,ForeignKey("user.id"),index=True)
    user=relationship("User",back_populates="order_products",lazy="selectin")


    

    product_id=Column("product_id",UUID,ForeignKey("product.id"),index=True)
    product=relationship("Product",back_populates="order_products",lazy="selectin")

    payment_id=Column("payment_id",UUID,ForeignKey("payment.id"),index=True)



    order_id=Column("order_id",UUID,ForeignKey("order.id"),index=True)
    order=relationship("Order",back_populates="order_products",lazy="selectin")

    quantity=Column(Integer,index=True)



class OrderCartItem(Base):

    __tablename__="order_cartitems"

    id=Column("id",Integer,index=True,unique=True,primary_key=True,)
    order_id=Column("order_id",UUID,ForeignKey("order.id"),index=True)
    item_id=Column("item_id",UUID,ForeignKey("cart_item.id"),index=True)

