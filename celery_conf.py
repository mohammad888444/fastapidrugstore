import os

from celery import Celery


# app = Celery('proj')
app=Celery("config",
           broker="redis://localhost:6655",
           backend="redis://localhost:6655",
           )


# app.config_from_object('django.conf:settings', namespace='CELERY')


app.autodiscover_tasks()



# @app.task(
#     name="first",
#     bnid=True
# )
# def send_email(self):
#     return "Hello"
