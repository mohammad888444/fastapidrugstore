from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import declarative_base,relationship
from sqlalchemy import Column,Integer,String,ForeignKey,DateTime,select





engine = create_async_engine('sqlite+aiosqlite:///backEnd.db')

Base = declarative_base()


SessionLocal = sessionmaker(
    expire_on_commit=False,
    class_=AsyncSession,
    bind=engine,
)



async def get_db() -> AsyncSession:
    
    async with engine.begin() as conn:
        
        await conn.run_sync(Base.metadata.create_all)
        db=SessionLocal()
        try:
            yield db
        finally:
            await db.close()

 

 

from models.account import User,UserOtp,Token,CustomizedSecretKey,BlockUser
from models.category import Category,Product,ProductImage,ProductMultImageManyToMany,ProductReview,UserFavaorates
from models.cart import Cart,CartItem
from models.orders import Payment,Order,OrderProduct,OrderCartItem


