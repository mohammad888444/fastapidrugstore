print("SSS")

from fastapi import (
    FastAPI,Header,HTTPException,Cookie,Body,
    Path,BackgroundTasks,Query,
    Response,Request,File,Form,UploadFile,
    Depends
    
)
from datetime import datetime

from fastapi.templating import  Jinja2Templates
from fastapi.staticfiles import StaticFiles
from database import get_db
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select


from routes.account import router as account_router
from routes.category import router as category_router
from routes.cart import router as cart_router
from routes.testWebsocket import router as web_router
from routes.reviews import router as review_router
from routes.profile import router as profile_router



from fastapi.middleware.cors import CORSMiddleware
from fastapi.middleware.httpsredirect import HTTPSRedirectMiddleware
from unicorn import UnicornMiddleware

from fastapi.middleware.trustedhost import TrustedHostMiddleware

from fastapi.middleware.gzip import GZipMiddleware




async def checkUserIsBlockedOrNot(
                    request:Request,
                    db:AsyncSession=Depends(get_db)
            ):
    

    from models.account import BlockUser
    user_ip=request.client.host
    now=datetime.now()
    b=await db.execute(
        select(
            BlockUser
        ).where(
            BlockUser.user_ip==user_ip,
            BlockUser.created>now
        ).limit(1)
    )
    b_sc=b.scalar()

    if b_sc:
        raise HTTPException(detail="user is blocked ",status_code=404)
    





app=FastAPI(
    deprecated=False,
    version="1.20",
    # dependencies=[Depends(checkUserIsBlockedOrNot)],
    description="drug store ",
    title="drug saling",
)


app.mount("/static",app=StaticFiles(directory="static"),name="static")

templates=Jinja2Templates(directory="/templates")


app.include_router(
    router=category_router,
    deprecated=False,
    include_in_schema=True,
    
)

 
app.include_router(
    router=account_router,
    prefix="/account",
    include_in_schema=True,
    deprecated=False
)



app.include_router(
    router=cart_router,
    prefix="/cart",
    include_in_schema=True,
    deprecated=False
)


app.include_router(
    router=web_router,
    prefix="/web_router",
    include_in_schema=True,
    deprecated=False,


)




app.include_router(
    router=review_router,
    prefix="/review",
    include_in_schema=True,
    deprecated=False,


)


app.include_router(
    router=profile_router,
    prefix="/profile",
    include_in_schema=True,
    deprecated=False,


)






origins = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost",
    "http://localhost:8080",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)




app.add_middleware(HTTPSRedirectMiddleware)


app.add_middleware(UnicornMiddleware, some_config="rainbow")

app.add_middleware(
    TrustedHostMiddleware, allowed_hosts=["example.com", "*.example.com"]
)
app.add_middleware(GZipMiddleware, minimum_size=1000)